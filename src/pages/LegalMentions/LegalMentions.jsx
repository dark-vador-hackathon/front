import styles from "./LegalMentions.module.scss"

export default function LegalMentions() {
    return(
        <div className={styles.container}>
        <h1>Mentions légales</h1>
      
        <h2>1. Informations générales</h2>
        <p>Ce site est exploité par DJ du Climat, une société enregistrée au 29 Rue Rosa Bonheur 33000 Bordeaux. Pour toute question ou demande concernant ces mentions légales, vous pouvez nous contacter à l'adresse suivante : djduclimat@gmail.com.</p>
      
        <h2>2. Collecte des données personnelles</h2>
        <h3>a. Données collectées</h3>
        <p>Lorsque vous utilisez notre jeu sur navigateur et son espace d'authentification, nous sommes susceptibles de collecter certaines données personnelles vous concernant, telles que :</p>
        <ul>
          <li>Votre nom et prénom</li>
          <li>Votre nom d'utilisateur</li>
          <li>Votre adresse e-mail</li>
          <li>Votre adresse postale</li>
          <li>Les informations de connexion (date, heure, durée, etc.)</li>
          <li>Votre préférence de langue</li>
        </ul>
      
        <h3>b. Base légale et finalités de la collecte</h3>
        <p>La collecte de vos données personnelles est fondée sur l'intérêt légitime de DJ DU CLIMAT à fournir et améliorer notre jeu sur navigateur. Les données collectées sont utilisées dans les buts suivants :</p>
        <ul>
          <li>Vous permettre de créer un compte et d'accéder à l'espace d'authentification</li>
          <li>Gérer votre compte utilisateur et vos préférences</li>
          <li>Assurer la sécurité du jeu et prévenir les activités frauduleuses</li>
          <li>Améliorer et personnaliser votre expérience de jeu</li>
          <li>Communiquer avec vous concernant des mises à jour, des promotions ou des enquêtes, avec votre consentement lorsque cela est requis par la loi</li>
        </ul>
      
        <h3>c. Durée de conservation des données</h3>
        <p>Vos données personnelles seront conservées pendant la durée nécessaire pour atteindre les finalités mentionnées ci-dessus, sauf si une période de conservation plus longue est exigée ou autorisée par la loi.</p>
      
        <h2>3. Cookies et technologies similaires</h2>
        <h3>a. Utilisation des cookies</h3>
        <p>Nous utilisons des cookies et des technologies similaires sur notre site pour améliorer votre expérience de jeu, personnaliser le contenu, analyser les performances du site et vous fournir des publicités ciblées. En utilisant notre jeu sur navigateur, vous consentez à l'utilisation de ces cookies, conformément à notre politique en matière de cookies.</p>
      
        <h3>b. Gestion des cookies</h3>
        <p>Vous pouvez choisir de désactiver les cookies à tout moment en modifiant les paramètres de votre navigateur. Cependant, veuillez noter que certaines fonctionnalités du jeu pourraient ne pas fonctionner correctement si les cookies sont désactivés.</p>
      
        <h2>4. Sécurité des données</h2>
        <p>Nous nous engageons à assurer la sécurité de vos données personnelles et à prendre les mesures techniques et organisationnelles appropriées pour prévenir tout accès non autorisé, toute divulgation, toute altération ou toute destruction de données. Nous limitons l'accès à vos données personnelles aux seules personnes qui ont besoin d'y accéder pour les finalités mentionnées dans ces mentions légales.</p>
      
        <h2>5. Vos droits</h2>
        <p>Conformément à la législation applicable en matière de protection des données, vous disposez des droits suivants :</p>
        <ul>
          <li>Le droit d'accéder à vos données personnelles et d'en obtenir une copie</li>
          <li>Le droit de rectifier toute donnée personnelle inexacte ou incomplète</li>
          <li>Le droit de demander l'effacement de vos données personnelles dans certaines circonstances</li>
          <li>Le droit de limiter le traitement de vos données personnelles dans certaines circonstances</li>
          <li>Le droit de vous opposer au traitement de vos données personnelles</li>
          <li>Le droit de retirer votre consentement lorsque le traitement de vos données personnelles est fondé sur votre consentement</li>
        </ul>
        <p>Pour exercer vos droits ou pour toute question concernant vos données personnelles, veuillez nous contacter à l'adresse djduclimat@gmail.com.</p>
      
        <h2>6. Modifications de la politique de confidentialité</h2>
        <p>Nous nous réservons le droit de modifier cette politique de confidentialité à tout moment. Les modifications entreront en vigueur dès leur publication sur cette page. Nous vous recommandons de consulter régulièrement cette page pour vous tenir informé des éventuelles modifications.</p>
      
      
      </div>
    )
}