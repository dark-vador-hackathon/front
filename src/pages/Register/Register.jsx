import { useState } from "react";
import * as Form from "@radix-ui/react-form";
import styles from "../Login/Login.module.scss";
import Button from "../../components/ui/Button/Button";
import { Link, useNavigate } from "react-router-dom";

export default function Register() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [firstName, setFirstName] = useState("")
  const [lastName, setLastName] = useState("")
  const navigate = useNavigate()

  function handleSubmit(e) {
    e.preventDefault();
    navigate("/game/1")
  }

  function handleInputChange(e) {
    const { name, value } = e.target;
    if (name === "email") {
      setEmail(value);
    } else if (name === "password") {
      setPassword(value);
    } else if (name === "firstName") {
      setFirstName(value);
    } else if (name === "lastName") {
      setLastName(value);
    }
  }

  return (
    <div className={styles.container}>
      <div className={styles.card}>
        <h2>Créer son compte</h2>
        <Form.Root className={styles.form} onSubmit={handleSubmit}>
          <Form.Field name="email">
            <div className={styles.formLabel}>
              <Form.Label>Email</Form.Label>
              <Form.Message match="valueMissing">
                Veuillez entrer votre email
              </Form.Message>
              <Form.Message match="typeMismatch">
                Veuillez entrer un email valide
              </Form.Message>
            </div>
            <Form.Control asChild>
              <input onChange={handleInputChange} className={styles.input} type="email" required />
            </Form.Control>
          </Form.Field>
          <Form.Field name="firstName">
            <div className={styles.formLabel}>
              <Form.Label >Prénom</Form.Label>
              <Form.Message match="valueMissing">
                Veuillez entrer votre prénom
              </Form.Message>
              <Form.Message match="typeMismatch">
                Veuillez entrer un prénom valide
              </Form.Message>
            </div>
            <Form.Control asChild>
              <input onChange={handleInputChange} className={styles.input} type="text" required />
            </Form.Control>
          </Form.Field>
          <Form.Field name="lastName">
            <div className={styles.formLabel}>
              <Form.Label>Nom</Form.Label>
              <Form.Message match="valueMissing">
                Veuillez entrer votre nom
              </Form.Message>
              <Form.Message match="typeMismatch">
                Veuillez entrer un nom valide
              </Form.Message>
            </div>
            <Form.Control asChild>
              <input onChange={handleInputChange} className={styles.input} type="text" required />
            </Form.Control>
          </Form.Field>
          <Form.Field name="password">
            <div className={styles.formLabel}>
              <Form.Label>Mot de passe</Form.Label>
              <Form.Message match="valueMissing">
                Veuillez entrer votre mot de passe
              </Form.Message>
              <Form.Message match="typeMismatch">
                Veuillez entrer un mot de passe valide
              </Form.Message>
            </div>
            <Form.Control asChild>
              <input onChange={handleInputChange} className={styles.input} type="password" required />
            </Form.Control>
          </Form.Field>
          <Form.Submit asChild>
            <Button type="submit">Créer un compte</Button>
          </Form.Submit>
        </Form.Root>
        <div className={styles.linkContainer}>
          <Link to="/login">Vous avez déjà un compte ? Connectez vous.</Link>
        </div>
      </div>
    </div>
  );
}
