import styles from "./Game.module.scss";
import { useState, useEffect, useRef } from "react";
import { useParams } from "react-router-dom";
import Mix from "../../components/MIX/Mix"
import CardsContainer from "../../components/CardsContainer/CardsContainer";
import questions from "../../data/questions.json"
import { useGameStatus } from "../../contexts/GameStatusContext";

export default function Game() {
  const { id } = useParams();
  // const [game, setGame] = useState(null);
  const [cards, setCards] = useState(questions.questions);
  const [loading, setLoading] = useState(true);
  const [progress, setProgress] = useState(0)
  const [temp, setTemp] = useState(3.3)
  const [mixType, setMixType] = useState("")
  const containerRef = useRef(null);

  const game = useGameStatus()

  useEffect(() => {
    initGame()
    fetchGame();
  }, []);

  async function fetchGame() {
    // TODO call API
    setLoading(false);
  }

  function initGame() {
    const initGameObject = {
      mix: [0, 0, 0],
      status: 1,
      temp: 3.3,
      currentCard: 1
    }

    game.setGame(initGameObject)
  }

  function saveGame(card, newProgress, mixType, newTempResult) {
    if (mixType == "M") {

      const currentGame =
      {
        mix: [newProgress, game.game.mix[1], game.game.mix[2]],
        status: 1,
        temp: newTempResult,
        currentCard: Number(card.id) + 1
      }
      game.setGame(currentGame)
    }
    if (mixType == "I") {

      const currentGame =
      {
        mix: [game.game.mix[0], newProgress, game.game.mix[2]],
        status: 1,
        temp: newTempResult,
        currentCard: Number(card.id) + 1
      }
      game.setGame(currentGame)
    }
    if (mixType == "X") {

      const currentGame =
      {
        mix: [game.game.mix[0], game.game.mix[1], newProgress],
        status: 1,
        temp: newTempResult,
        currentCard: Number(card.id) + 1
      }
      game.setGame(currentGame)
    }


  }

  function handleCardSwipe(card, mixResult, mixType, tempResult) {
    const newProgress = progress + mixResult;
    const newTempResult = Math.round((temp + tempResult) * 10) / 10;
    const newCards = cards.filter((c) => c.id !== card.id);

    setCards(newCards);
    setMixType(mixType);
    setProgress(newProgress)
    setTemp(newTempResult)
    saveGame(card, newProgress, mixType, newTempResult)

    if (mixResult < 0) {
      if (containerRef.current) {
        containerRef.current.classList.add(styles.shake);
        setTimeout(() => {
          containerRef.current.classList.remove(styles.shake);
        }, 200);
      }
    }
  }

  if (loading) {
    return <h2>Chargement...</h2>;
  }

  return (
    <div className={styles.container} ref={containerRef}>
      <Mix progress={progress} type={mixType} />
      <CardsContainer
        cards={cards}
        onSwipe={handleCardSwipe}
      />
    </div>
  );
}
