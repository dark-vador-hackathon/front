import { Link } from 'react-router-dom';
import styles from './Home.module.scss';
import Button from '../../components/ui/Button/Button';

export default function Home() {
  return (
    <div className={styles.home}>
      <h1 className={styles.title}>Bienvenue sur le jeu MIX Climatique</h1>
      <section>
        “Mix Climatique“ est un jeu en ligne innovant qui plonge les joueurs dans la réalité du défi climatique. En tenant compte des conditions actuelles et futures du climat au niveau mondial et local, le jeu guide les joueurs dans l'élaboration de stratégies climatiques pour construire l'avenir de leur choix. Les joueurs acquièrent des connaissances sur les engagements actuels pour le climat et les tendances futures si des actions significatives ne sont pas entreprises. Connecté au simulateur EnROADS, le jeu permet aux utilisateurs de voir l'impact de leurs choix actuels sur l'avenir, offrant une expérience ludique et éducative qui souligne les opportunités et les défis de la lutte contre le changement climatique. <br></br><br></br> <strong>VOTRE BUT : NE PAS DEPASSER LES +1.5 DEGRES EN 2100 !</strong>
      </section>
      <div className={styles.buttons}>
        <Link to="/game/1">
          <Button>Jouer</Button>
        </Link>
        <Link to="/login">
          <Button>Se connecter</Button>
        </Link>
      </div>
      <img src='/images/mix-picture.png' alt='mix picture' className={styles.picture} />
    </div>
  )
}