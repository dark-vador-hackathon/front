import styles from "./Results.module.scss";
import { useState, useEffect, useContext } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { GameStatusContext } from '../../contexts/GameStatusContext';
import fire from '../../assets/fire.gif'
import trophy from '../../assets/trophy.gif'




export default function Results() {
    const { id } = useParams();
    const [result, setResult] = useState(null);
    const { game } = useContext(GameStatusContext)

    const navigate = useNavigate()

    function isGameExist() {
        if (game == null) {
            navigate("/")
        }
    }

    useEffect(() => {
        isGameExist()

    })

    return (
        <>
            {game != null ?
                <>
                    {game.temp <= 1.5 ?
                        <div className={styles.container}>
                            <div className={`${styles.card} ${styles.safe}`}>

                                <h2>BIEN JOUÉ</h2>
                                <img className={styles.gif} src={trophy} alt="trophy gif" />
                                <p>Température en 2100 : <strong className={styles.win}>+{game.temp}°C</strong><br></br>Bravo, vous avez atteint l‘objectif !</p>

                            </div>
                        </div>
                        :
                        <div className={styles.container}>
                            <div className={`${styles.card} ${styles.fire}`}>

                                <h2>PERDU</h2>
                                <img className={styles.gif} src={fire} alt="fire gif" />
                                <p>Température en 2100 : <strong className={styles.loose}>+{game.temp}°C</strong><br></br>Il faut encore faire quelques efforts pour atteindre l‘objectif</p>

                            </div>
                        </div>
                    }
                </>

                :
                <></>

            }

        </>
    )
}