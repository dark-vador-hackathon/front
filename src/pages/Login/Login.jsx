import { useState } from "react";
import * as Form from "@radix-ui/react-form";
import styles from "./Login.module.scss";
import Button from "../../components/ui/Button/Button";
import { Link, redirect, useNavigate } from "react-router-dom";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate()


  function handleSubmit(e) {
    e.preventDefault();
    navigate("/game/1")

  }

  function handleInputChange(e) {
    const { name, value } = e.target;
    if (name === "email") {
      setEmail(value);
    } else if (name === "password") {
      setPassword(value);
    }
  }

  return (
    <div className={styles.container}>
      <div className={styles.card}>
        <h2>Se connecter</h2>
        <Form.Root className={styles.form} onSubmit={handleSubmit}>
          <Form.Field name="email">
            <div className={styles.formLabel}>
              <Form.Label>Email</Form.Label>
              <Form.Message match="valueMissing">
                Veuillez entrer votre email
              </Form.Message>
              <Form.Message match="typeMismatch">
                Veuillez entrer un email valide
              </Form.Message>
            </div>
            <Form.Control asChild>
              <input onChange={handleInputChange} className={styles.input} type="email" required />
            </Form.Control>
          </Form.Field>
          <Form.Field name="password">
            <div className={styles.formLabel}>
              <Form.Label>Mot de passe</Form.Label>
              <Form.Message match="valueMissing">
                Veuillez entrer votre mot de passe
              </Form.Message>
              <Form.Message match="typeMismatch">
                Veuillez entrer un mot de passe valide
              </Form.Message>
            </div>
            <Form.Control asChild>
              <input onChange={handleInputChange} className={styles.input} type="password" required />
            </Form.Control>
          </Form.Field>
          <Form.Submit asChild>
            <Button type="submit">
              Se connecter
            </Button>
          </Form.Submit>
        </Form.Root>
        <div className={styles.linkContainer}>
          <Link to="/register">Vous n‘avez pas compte ? Créez en un.</Link>
        </div>
      </div>
    </div>
  );
}
