import { createContext, useState, useContext } from 'react';
import PropTypes from "prop-types";


export const GameStatusContext = createContext({
    game: {
        mix: [0, 0, 0],
        status: 1,
        temp: 3.3,
        currentCard: 1
    }
    ,
    setGame: (game) => { },
});

// Object Game
// {
//     mix: [int, int,int],
//     status: int,
//     temp: double,
//     currentCard: int
// }


export default function GameStatusProvider({ children }) {

    const [game, setGame] = useState(null);

    const status = {
        game,
        setGame
    };

    return (
        <GameStatusContext.Provider value={status}>
            {children}
        </GameStatusContext.Provider>
    );
}

GameStatusProvider.propTypes = {
    children: PropTypes.node.isRequired,
}

// eslint-disable-next-line react-refresh/only-export-components
export const useGameStatus = () => useContext(GameStatusContext);
