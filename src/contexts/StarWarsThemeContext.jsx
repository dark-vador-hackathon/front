import { createContext, useState, useContext } from 'react';
import PropTypes from "prop-types";
import { useEffect, useRef } from 'react';


export const StarWarsThemeContext = createContext({
  isSWMode: false,
  toggleDarkMode: () => {},
});

export default function StarWarsThemeProvider({ children }) {
  const [isSWMode, setIsSWMode] = useState(false);
  const audioRef = useRef(null);
  const lightSaberCursor = useRef(null);
  const lightSaberAudio = useRef(null);
  const lightSaberAudioClick = useRef(null);
  const formerCursor = useRef(null);

  useEffect(() => {
    audioRef.current = new Audio("/sounds/swtheme.mp3");
    audioRef.current.loop = true;
    audioRef.current.volume = 0.5;
    lightSaberAudio.current = new Audio("/sounds/lightsaber.mp3");
    lightSaberAudioClick.current = new Audio("/sounds/lightsaberclick.mp3");
  }, [])

  useEffect(() => {
    const handleCursorMove = (e) => {
      const cursor = document.querySelector("#light-saber-cursor");
      cursor.style.left = `calc(${e.clientX}px - 15px)`;
      cursor.style.top = `calc(${e.clientY}px - 15px)`;

      if (formerCursor.current) {
        const distance = Math.sqrt(
          Math.pow(e.clientX - formerCursor.current.x, 2) +
          Math.pow(e.clientY - formerCursor.current.y, 2)
        );

        if (distance > 10 && isSWMode) {
          lightSaberAudio.current?.play();
        }
      }

      formerCursor.current = { x: e.clientX, y: e.clientY };
    }

    const handleCursorClick = (e) => {
      if (isSWMode) {
        lightSaberAudioClick.current?.play();
      }
    }

    document.addEventListener("mousemove", handleCursorMove);
    document.addEventListener("click", handleCursorClick);

    return () => {
      document.removeEventListener("mousemove", handleCursorMove);
      document.removeEventListener("click", handleCursorClick);
    }
  });

  useEffect(() => {
    const body = document.querySelector("body");

    if (isSWMode) {
      body.classList.add("SWTheme")
      audioRef.current?.play();
    } else {
      body.classList.remove("SWTheme")
      audioRef.current?.pause();
    }
  }, [isSWMode])

  const toggleDarkMode = () => {
    setIsSWMode(!isSWMode);
  };

  const theme = {
    isSWMode,
    toggleDarkMode,
  };

  return (
    <StarWarsThemeContext.Provider value={theme}>
      <img src='/images/light-saber.png' id="light-saber-cursor" ref={lightSaberCursor}/>
      {children}
    </StarWarsThemeContext.Provider>
  );
}

StarWarsThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
}

// eslint-disable-next-line react-refresh/only-export-components
export const useStarWarsTheme = () => useContext(StarWarsThemeContext);