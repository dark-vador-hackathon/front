import styles from "./CardSwipable.module.scss";
import { useRef } from "react";
import Button from "../ui/Button/Button";
import PropTypes from "prop-types";

export default function CardSwipable({ card, onSwipeLeft, onSwipeRight, leftText, rightText }) {
  const cardRef = useRef(null);
  const swipeTimeout = 1000;

  function handleSwipeLeft() {
    if (cardRef.current) {
      cardRef.current.classList.add(styles.swipeLeft);
      setTimeout(() => {
        cardRef.current.classList.remove(styles.swipeLeft);
      }, swipeTimeout)
    }
    onSwipeLeft();
  }

  function handleSwipeRight() {
    if (cardRef.current) {
      cardRef.current.classList.add(styles.swipeRight);
      setTimeout(() => {
        cardRef.current.classList.remove(styles.swipeRight);
      }, swipeTimeout)
    }
    onSwipeRight();
  }

  return (
    <div className={styles.container}>
      <div className={styles.card} ref={cardRef}>

      </div>
      <div className={styles.buttons}>
        <Button onClick={handleSwipeLeft}>{leftText}</Button>
        <Button onClick={handleSwipeRight}>{rightText}</Button>
      </div>
    </div>
  )
}

CardSwipable.propTypes = {
  card: PropTypes.object,
  onSwipeLeft: PropTypes.func,
  onSwipeRight: PropTypes.func,
  leftText: PropTypes.string,
  rightText: PropTypes.string,
}