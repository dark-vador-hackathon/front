import { Link } from 'react-router-dom';
import styles from "./Footer.module.scss";
export default function Footer() {
  return (
    <footer>
      <p className={styles.copyright}>© 2023 Dark-Vador-team all rights reserved</p>
      <Link className={ styles.MentionsLegal } to="/legal-mentions">Mentions Legal</Link>
    </footer>
  )
}