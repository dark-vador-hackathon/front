import React from 'react';
import styles from './ProgressBar.module.scss';


const ProgressBar = (props) => {

    const progress = props.progress;

    let mix = props.mix

    let type = props.type

    const indicator = [
        [
            "Discret",
            "Progressif",
            "Surprenant",
            "Audacieux",
            "Disruptif"
        ],

        [
            "Discret",
            "People",
            "Minimaliste",
            "Emergent",
            "Evolutif",
            "Revolutionnaire"
        ],

        [
            "Pausé",
            "Fondamental",
            "Créatif",
            "Innovant"
        ]

    ]



    return (
        <div className={styles.container}>
            <span className={styles.mixTitle}>{mix}</span>
            <div className={styles.progressContainer}>
                <div className={styles.ProgressRoot}>
                    <div
                        className={`${styles.ProgressIndicator} ${styles[mix]}`}
                        style={{ transform: `translateX(-${98 - progress}%)` }}
                    >
                    </div>
                </div>

                {type == 0 && progress < 13 ?
                    <p className={styles.indicator}>{indicator[type][0]}</p>
                    :
                    <></>
                }
                {type == 0 && 13 <= progress && progress < 26 ?
                    <p className={styles.indicator}>{indicator[type][1]}</p>
                    :
                    <></>
                }
                {type == 0 && 26 <= progress && progress < 61 ?
                    <p className={styles.indicator}>{indicator[type][2]}</p>
                    :
                    <></>
                }
                {type == 0 && 61 <= progress && progress < 100 ?
                    <p className={styles.indicator}>{indicator[type][3]}</p>
                    :
                    <></>
                }
                {type == 0 && progress == 100 ?
                    <p className={styles.indicator}>{indicator[type][4]}</p>
                    :
                    <></>
                }


                {type == 1 && progress <= 0 ?
                    <p className={styles.indicator}>{indicator[type][0]}</p>
                    :
                    <></>
                }
                {type == 1 && 4 <= progress && progress < 20 ?
                    <p className={styles.indicator}>{indicator[type][1]}</p>
                    :
                    <></>
                }
                {type == 1 && 20 <= progress && progress < 28 ?
                    <p className={styles.indicator}>{indicator[type][2]}</p>
                    :
                    <></>
                }
                {type == 1 && 28 <= progress && progress < 48 ?
                    <p className={styles.indicator}>{indicator[type][3]}</p>
                    :
                    <></>
                }
                {type == 1 && 48 <= progress && progress < 100 ?
                    <p className={styles.indicator}>{indicator[type][4]}</p>
                    :
                    <></>
                }
                {type == 1 && progress == 100 ?
                    <p className={styles.indicator}>{indicator[type][5]}</p>
                    :
                    <></>
                }


                {type == 2 && progress <= 0 ?
                    <p className={styles.indicator}>{indicator[type][0]}</p>
                    :
                    <></>
                }
                {type == 2 && 13 <= progress && progress < 63 ?
                    <p className={styles.indicator}>{indicator[type][1]}</p>
                    :
                    <></>
                }
                {type == 2 && 63 <= progress && progress < 100 ?
                    <p className={styles.indicator}>{indicator[type][2]}</p>
                    :
                    <></>
                }

                {type == 2 && 100 <= progress ?
                    <p className={styles.indicator}>{indicator[type][4]}</p>
                    :
                    <></>
                }
            </div>

        </div>
    );
}

export default ProgressBar;