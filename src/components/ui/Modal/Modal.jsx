import { useRef, useEffect } from 'react';
import styles from './Modal.module.scss';
import PropTypes from 'prop-types';

export default function Modal({ isOpen, onClose, children }) {
  const modalRef = useRef(null);

  useEffect(() => {
    function handleEscape(event) {
      if (event.key === "Escape") {
        onClose();
      }
    }
    if (isOpen) {
      document.addEventListener("keydown", handleEscape);
    }
    return () => {
      document.removeEventListener("keydown", handleEscape);
    };
  }, [isOpen, onClose]);

  useEffect(() => {

    if (modalRef.current) {

     modalRef.current.removeAttribute('open')
     
      if (isOpen) {
        modalRef.current.showModal();
      }else  {
        modalRef.current.close();
      }
    }
  }, [isOpen]);

  return (
    <dialog
      ref={modalRef}
      className={styles.modal}
    >
      {children}
    </dialog>
  )
}

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};