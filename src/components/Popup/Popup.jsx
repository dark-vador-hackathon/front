import { useState, useEffect } from 'react';
import { setCookie, getCookie } from '../Cookie/Cookie';
import styles from './Popup.module.scss';
import Modal from '../ui/Modal/Modal';
import Button from "../../components/ui/Button/Button";

function PopupCookie() {
  const [showPopup, setShowPopup] = useState(false);

  const acceptCookie = () => {
    setShowPopup(false);
    setCookie('cookies', 'accepted', 30);
  };

  const refuseCookie = () => {
    setShowPopup(false);
    setCookie('cookies', 'refused', 30);
  }

  useEffect(() => {
    const cookies = getCookie('cookies');
    if (cookies) {
      setShowPopup(false);
    } else {
      setShowPopup(true);
    }
  }, []);

  return (
    <Modal isOpen={showPopup} onClose={() => setShowPopup(false)}>
      <div>
        <p>Ce site utilise des cookies pour améliorer votre expérience. En continuant à naviguer sur ce site, vous acceptez notre utilisation des cookies.</p>
        <div className={styles.buttons}>
          <Button variant="outlined" onClick={refuseCookie}>Refuser</Button>
          <Button onClick={acceptCookie}>Accepter</Button>
        </div>
      </div>
    </Modal>
  );
}

export default PopupCookie;
