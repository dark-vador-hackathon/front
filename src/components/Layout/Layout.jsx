import PropTypes from "prop-types";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import PopupCookie from "../Popup/Popup";
import StarWarsThemeProvider from "../../contexts/StarWarsThemeContext";
import GameStatusProvider from "../../contexts/GameStatusContext";

export default function Layout({ children }) {
  return (
    <GameStatusProvider>
      <StarWarsThemeProvider>
        <PopupCookie />
        <Header />
        <main>
          {children}
        </main>
        <Footer />
      </StarWarsThemeProvider>
    </GameStatusProvider>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}