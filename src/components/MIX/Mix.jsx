import { useEffect, useState } from "react";
import ProgressBar from "../ui/ProgressBar/ProgressBar"
import styles from "./Mix.module.scss";


const Mix = ({ progress, type }) => {

    const [progressM, setProgressM] = useState(0)
    const [progressI, setProgressI] = useState(0)
    const [progressX, setProgressX] = useState(0)


    useEffect(() => {
        if (type == "M") {
            setProgressM(progressM + progress)
        };
        if (type == "I") {
            setProgressI(progressI + progress)
        };
        if (type == "X") {
            setProgressX(progressX + progress)
        }
    }, [progress])


    return (
        <div className={styles.card}>
            <ProgressBar progress={progressM} type={0} mix={"M"} />
            <ProgressBar progress={progressI} type={1} mix={"I"} />
            <ProgressBar progress={progressX} type={2} mix={"X"} />

        </div>
    )
}

export default Mix