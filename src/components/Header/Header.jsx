import styles from "./Header.module.scss";
import { Link } from "react-router-dom";
import { useStarWarsTheme } from "../../contexts/StarWarsThemeContext";
import * as Switch from "@radix-ui/react-switch";

export default function Header() {
  const { toggleDarkMode, isSWMode } = useStarWarsTheme();

  return (
    <header className={styles.header}>
      <Link to="/">
        <img className={styles.logo} src="/logo.png" alt="Logo" />
      </Link>

      <label className={styles.starsWarsModeContainer}>
        Star wars mode
        <Switch.Root
          className={styles.switchRoot}
          checked={isSWMode}
          onClick={toggleDarkMode}
        >
          <Switch.Thumb
            className={styles.switchThumb}
          />
        </Switch.Root>
      </label>
    </header>
  );
}
