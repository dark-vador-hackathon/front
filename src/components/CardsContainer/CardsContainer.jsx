import styles from "./CardsContainer.module.scss";
import Button from "../ui/Button/Button";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

export default function CardsContainer({ cards, onSwipe }) {
  const swipeTimeout = 980;

  function handleSwipeLeft(card) {
    const cardNode = document.getElementById(`Card_${card.id}`);
    if (cardNode) {
      cardNode.classList.add(styles.swipeLeft);
      setTimeout(() => {
        cardNode.classList.remove(styles.swipeLeft);
        onSwipe(card, card.mixResult[0], card.mixType, card.tempResult[0]);
      }, swipeTimeout);
    }
  }

  function handleSwipeRight(card) {
    const cardNode = document.getElementById(`Card_${card.id}`);
    if (cardNode) {
      cardNode.classList.add(styles.swipeRight);
      setTimeout(() => {
        cardNode.classList.remove(styles.swipeRight);
        onSwipe(card, card.mixResult[1], card.mixType, card.tempResult[1]);
      }, swipeTimeout);
    }
  }

  return (
    <div className={styles.container}>
      {cards.map((card, index) => (
        <div
          className={styles.card}
          id={`Card_${card.id}`}
          key={card.id}
          style={{
            transform: `translateX(${index < 3 ? index * 10 : 20
              }px) translateY(${index < 3 ? index * -10 : -20}px)`,
            zIndex: index < 3 ? 3 - index : 0,
          }}
        >
          <h3 className={styles.title}>{card.question}</h3>
          <img className={styles.icon} src={`../../../public/pictures/${card.category}/${card.category}.png`}></img>
          <div className={styles.buttons}>
            <Button
              disabled={index !== 0}
              onClick={() => handleSwipeLeft(card)}
            >
              {card.response[0]}
            </Button>
            <Button
              disabled={index !== 0}
              onClick={() => handleSwipeRight(card)}
            >
              {card.response[1]}
            </Button>
          </div>
        </div>
      ))}
      <div
        className={styles.card}
        style={{
          transform: `translateX(10px) translateY(-10px)`,
          zIndex: 0,
        }}
      >
        <h3 className={styles.title}>Fin de la partie</h3>
        <div className={styles.buttons}>
          <Link to="/results/1">
            <Button>Voir les résultats</Button>
          </Link>
        </div>
      </div>
    </div>
  );
}

CardsContainer.propTypes = {
  cards: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      question: PropTypes.string.isRequired,
      response: PropTypes.array.isRequired,
    })
  ),
  onSwipe: PropTypes.func,
};
