import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Home from "./pages/Home/Home";
import Login from "./pages/Login/Login";
import Register from "./pages/Register/Register";
import Layout from "./components/Layout/Layout";
import Game from "./pages/Game/Game";
import Results from "./pages/Results/Results";
import LegalMentions from "./pages/LegalMentions/LegalMentions";


export default function App() {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/game/:id" element={<Game />} />
          <Route path="/results/:id" element={<Results />} />
          <Route path="legal-mentions" element={<LegalMentions />} />
        </Routes>
      </Layout>
    </Router>
  );
}
